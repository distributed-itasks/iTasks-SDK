definition module Tests.Unit.Misc
/**
* Miscellaneous unit tests that don't fit anywhere yet, but we don't want to lose.
*/
import TestFramework

testMisc :: TestSuite
