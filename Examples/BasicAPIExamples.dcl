definition module BasicAPIExamples
import iTasks, iTasks.API.Extensions.Admin.WorkflowAdmin
/**
* This module contains a series of small examples of basic usage of the iTasks API.
*/
basicAPIExamples :: [Workflow]
