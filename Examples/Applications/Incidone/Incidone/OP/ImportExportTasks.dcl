definition module Incidone.OP.ImportExportTasks
/**
* This module provides tasks for mass exporting and importing
* parts of the operational picture data
*/
import iTasks

importContactsFromCSVFile :: Document -> Task ()
