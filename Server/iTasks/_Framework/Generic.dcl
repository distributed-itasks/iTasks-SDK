definition module iTasks._Framework.Generic

import iTasks._Framework.Generic.Visualization
import iTasks._Framework.Generic.Defaults
import iTasks.UI.Editor.Generic
import GenEq

//The iTask context restriction contains all generic functions that need to
//be available for a type to be used in tasks
class iTask a
	//Interaction
	| gEditor{|*|}
	//Visualization
	, gText{|*|}
	//Serialization
	, JSONEncode{|*|}
	, JSONDecode{|*|}
	//Data
	, gDefault{|*|}
	, gEq{|*|}
	, TC a
