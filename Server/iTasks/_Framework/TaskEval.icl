implementation module iTasks._Framework.TaskEval

import StdList, StdBool, StdTuple, StdMisc, StdDebug
import Data.Error, Data.Func, Data.Tuple, Data.Either, Data.Functor, Data.List, Text, Text.JSON
import iTasks._Framework.IWorld, iTasks._Framework.Task, iTasks._Framework.TaskState
import iTasks._Framework.Store, iTasks._Framework.TaskStore, iTasks._Framework.Util, iTasks._Framework.Generic
import iTasks.API.Core.Types
import iTasks.UI.Layout
import iTasks._Framework.SDSService

from iTasks.API.Core.TaskCombinators	import :: ParallelTaskType(..), :: ParallelTask(..)
from Data.Map as DM				        import qualified newMap, fromList, toList, get, put, del 
from Data.Queue import :: Queue (..)
from Data.Queue as DQ					import qualified newQueue, enqueue, dequeue, empty
from iTasks._Framework.SDS as SDS       import qualified read, write, modify
from iTasks.API.Common.SDSCombinators   import sdsFocus, >+|, mapReadWrite, mapReadWriteError
from StdFunc import const

derive gEq TIMeta

getNextTaskId :: *IWorld -> (!TaskId,!*IWorld)
getNextTaskId iworld=:{current=current=:{TaskEvalState|taskInstance,nextTaskNo}}
    = (TaskId taskInstance nextTaskNo, {IWorld|iworld & current = {TaskEvalState|current & nextTaskNo = nextTaskNo + 1}})

processEvents :: !Int *IWorld -> *(!MaybeError TaskException (), !*IWorld)
processEvents max iworld
	| max <= 0 = (Ok (), iworld)
	| otherwise
		= case dequeueEvent iworld of 
			(Nothing,iworld) = (Ok (),iworld)
			(Just (instanceNo,event),iworld)
				= case evalTaskInstance instanceNo event iworld of 
					(Ok taskValue,iworld)
						= processEvents (max - 1) iworld
					(Error msg,iworld=:{IWorld|world})
						# world = show ["WARNING: "+++ msg] world	
						= (Ok (),{IWorld|iworld & world = world})

//Evaluate a single task instance
evalTaskInstance :: !InstanceNo !Event !*IWorld -> (!MaybeErrorString (TaskValue JSONNode),!*IWorld)
evalTaskInstance instanceNo event iworld
    # iworld            = mbResetUIState instanceNo event iworld
    # (res,iworld)      = evalTaskInstance` instanceNo event iworld
    = (res,iworld)
where
    evalTaskInstance` instanceNo event iworld=:{clocks={localDate,localTime},current}
    # (constants, iworld)       = 'SDS'.read (sdsFocus instanceNo taskInstanceConstants) iworld
	| isError constants         = ((\(Error (e,msg)) -> Error msg) constants, iworld)
	# constants=:{InstanceConstants|instanceKey,session,listId} = fromOk constants
	# (oldReduct, iworld)		= 'SDS'.read (sdsFocus instanceNo taskInstanceReduct) iworld
	| isError oldReduct			= ((\(Error (e,msg)) -> Error msg) oldReduct, iworld)
	# oldReduct=:{TIReduct|task=Task eval,tree,nextTaskNo=curNextTaskNo,nextTaskTime,tasks,tonicRedOpts} = fromOk oldReduct
    # (oldProgress,iworld)      = 'SDS'.read (sdsFocus instanceNo taskInstanceProgress) iworld
	| isError oldProgress       = ((\(Error (e,msg)) -> Error msg) oldProgress, iworld)
    # oldProgress=:{InstanceProgress|value,attachedTo} = fromOk oldProgress
    //Check exeption
    | value === Exception
	    # (oldValue, iworld)		= 'SDS'.read (sdsFocus instanceNo taskInstanceValue) iworld
        = case oldValue of
            (Error (e,msg))             = (Error msg, iworld)
		    (Ok (TIException e msg))    = (Error msg, iworld)
            (Ok _)                      = (Error "Exception no longer available", iworld)
    //Eval instance
    # (currentSession,currentAttachment) = case (session,attachedTo) of
        (True,_)                                  = (Just instanceNo,[])
        (_,[])                                    = (Nothing,[])
        (_,attachment=:[TaskId sessionNo _:_])    = (Just sessionNo,attachment)
	//Update current process id & eval stack in iworld
	# taskId					= TaskId instanceNo 0
	# iworld					= {iworld & current =
                                        { taskInstance = instanceNo
                                        , sessionInstance = currentSession
                                        , attachmentChain = currentAttachment
										, taskTime = oldReduct.TIReduct.nextTaskTime
                                        , nextTaskNo = oldReduct.TIReduct.nextTaskNo
										}}
	//Apply task's eval function and take updated nextTaskId from iworld
	# (newResult,iworld=:{current})	= eval event {mkEvalOpts & tonicOpts = tonicRedOpts} tree iworld
    # tree                      = case newResult of
        (ValueResult _ _ _ newTree)  = newTree
        _                            = tree
    //Reset necessary 'current' values in iworld
    # iworld = {IWorld|iworld & current = {TaskEvalState|current & taskInstance = 0}}
    // Check if instance was deleted by trying to reread the instance constants share
	# (deleted,iworld) = appFst isError ('SDS'.read (sdsFocus instanceNo taskInstanceConstants) iworld)
    // Write the updated progress
	# (mbErr,iworld) = if (updateProgress (toDateTime localDate localTime) newResult oldProgress === oldProgress)
		(Ok (),iworld)	//Only update progress when something changed
   		('SDS'.modify (\p -> ((),updateProgress (toDateTime localDate localTime) newResult p)) (sdsFocus instanceNo taskInstanceProgress) iworld)
    = case mbErr of
        Error (e,msg)          = (Error msg,iworld)
        Ok _
            //Store updated reduct
            # (nextTaskNo,iworld)		= getNextTaskNo iworld
            # (_,iworld)                = 'SDS'.modify (\r -> ((),{TIReduct|r & tree = tree, nextTaskNo = nextTaskNo, nextTaskTime = nextTaskTime + 1}))
                                                (sdsFocus instanceNo taskInstanceReduct) iworld
												//FIXME: Don't write the full reduct (all parallel shares are triggered then!)
            //Store update value
            # newValue                  = case newResult of
                (ValueResult val _ _ _)     = TIValue val
                (ExceptionResult (e,str))   = TIException e str
            # (mbErr,iworld)            = if deleted (Ok (),iworld) ('SDS'.write newValue (sdsFocus instanceNo taskInstanceValue) iworld)
            = case mbErr of
                Error (e,msg)          = (Error msg,iworld)
                Ok _
                	= case newResult of
                    	(ValueResult value _ change _)	
							| deleted
								= (Ok value,iworld)
							//Only queue UI changes if something interesting is changed
							= case compactUIChange change of
								NoChange = (Ok value,iworld)
								change
									# iworld = queueUIChange instanceNo change iworld
                        			# iworld = flushShareCache iworld
									= (Ok value, iworld)
                    	(ExceptionResult (e,msg))
							= (Error msg, iworld)

	getNextTaskNo iworld=:{IWorld|current={TaskEvalState|nextTaskNo}}	    = (nextTaskNo,iworld)

	updateProgress now result progress
        # attachedTo = case progress.InstanceProgress.attachedTo of //Release temporary attachment after first evaluation
            (Just (_,[]))   = Nothing
            attachment      = attachment
		# progress = {InstanceProgress|progress
					 &firstEvent = Just (fromMaybe now progress.InstanceProgress.firstEvent)
					 ,lastEvent = Just now
					 }
		= case result of
			(ExceptionResult _)				    = {InstanceProgress|progress & value = Exception}
			(ValueResult (Value _ stable) _  _ _)	
                = {InstanceProgress|progress & value = if stable Stable Unstable}
			(ValueResult _ _ _ _)	
                = {InstanceProgress|progress & value = None}
			_									= {InstanceProgress|progress & value = None}

    mbResetUIState instanceNo ResetEvent iworld 
		# (_,iworld) = 'SDS'.write 'DQ'.newQueue (sdsFocus instanceNo taskInstanceUIChanges) iworld 
		//Remove all js compiler state for this instance
		# iworld=:{jsCompilerState=jsCompilerState} = iworld
		# jsCompilerState = fmap (\state -> {state & skipMap = 'DM'.del instanceNo state.skipMap}) jsCompilerState
		# iworld = {iworld & jsCompilerState = jsCompilerState}
		= iworld

    mbResetUIState _ _ iworld = iworld

updateInstanceLastIO ::![InstanceNo] !*IWorld -> *(!MaybeError TaskException (), !*IWorld)
updateInstanceLastIO [] iworld = (Ok (),iworld)
updateInstanceLastIO [instanceNo:instanceNos] iworld=:{IWorld|clocks={localDate,localTime}}
    = case 'SDS'.modify (\io -> ((),fmap (appSnd (const (toDateTime localDate localTime))) io)) (sdsFocus instanceNo taskInstanceIO) iworld of
    	(Ok (),iworld) = updateInstanceLastIO instanceNos iworld
		(Error e,iworld) = (Error e,iworld)

updateInstanceConnect :: !String ![InstanceNo] !*IWorld -> *(!MaybeError TaskException (), !*IWorld)
updateInstanceConnect client [] iworld = (Ok (),iworld)
updateInstanceConnect client [instanceNo:instanceNos] iworld=:{IWorld|clocks={localDate,localTime}}
    = case 'SDS'.write (Just (client,toDateTime localDate localTime)) (sdsFocus instanceNo taskInstanceIO) iworld of
		(Ok (),iworld) = updateInstanceConnect client instanceNos iworld
		(Error e,iworld) = (Error e,iworld)

updateInstanceDisconnect :: ![InstanceNo] !*IWorld -> *(!MaybeError TaskException (), !*IWorld)
updateInstanceDisconnect [] iworld = (Ok (),iworld)
updateInstanceDisconnect [instanceNo:instanceNos] iworld=:{IWorld|clocks={localDate,localTime}}
    = case 'SDS'.modify (\io -> ((),fmap (appSnd (const (toDateTime localDate localTime))) io)) (sdsFocus instanceNo taskInstanceIO) iworld of
		(Ok (),iworld) = updateInstanceDisconnect instanceNos iworld
		(Error e,iworld) = (Error e,iworld)

currentInstanceShare :: ReadOnlyShared InstanceNo
currentInstanceShare = createReadOnlySDS (\() iworld=:{current={TaskEvalState|taskInstance}} -> (taskInstance,iworld))
