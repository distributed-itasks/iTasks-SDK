definition module iTasks.API.Extensions.Editors.Ace
/**
* Integration of Cloud9 Ace code editor
*/

import iTasks
import iTasks.UI.Editor

aceTextArea :: Editor String
