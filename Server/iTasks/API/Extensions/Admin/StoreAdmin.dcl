definition module iTasks.API.Extensions.Admin.StoreAdmin
/**
* This module provides management tasks for the internal generic
* store of an iTasks applications.
*/
import iTasks

manageStore :: Task ()
